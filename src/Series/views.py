from django.shortcuts import render
from django.views.generic import ListView, DetailView, View

from .models import Series, Episodes
from Plans.models import UserPlan
# Create your views here.



class SeriesListView(ListView):
	model 			= Series



class SeriesDetailView(DetailView):
	model 			= Series



class EpisodesDetailView(View):
	"""
		detialview for the episodes, it inherits from View, since 
		we intend for this view to be customizable
	"""

	def get(self, request, series_slug, episode_slug, *args, **kwargs):
		"""
			We will filter all the series be some slug,
			Then filter all the episodes from the series result by some slug
		"""

		templates 	= "Series/episode_detail.html"

		series_qs 	= Series.objects.filter(slug=series_slug)
		if series_qs.exists():
			series 	= series_qs.first()

		episode_qs 	= series.episodes.filter(slug=episode_slug)
		if episode_qs.exists():
			episode = episode_qs.first()


		#Check user plan_type
		user_plan 		= UserPlan.objects.filter(user=request.user).first()
		user_plan_type 	= user_plan.plan.plan_type

		#get allowed series for the given plan
		series_allowed_plan_type 	= series.allowed_plans.all()

		context						= {
					'episode_object': None
		}


		#check if user can view series
		if series_allowed_plan_type.filter(plan_type=user_plan_type).exists():
			context 	= {
					'episode_object': episode
			}

		return render(request, templates, context)