from django.db import models
from django.urls import reverse


from Plans.models import Plans
# Create your models here.
class Series(models.Model):
	slug				= models.SlugField()
	title				= models.CharField(max_length=100)
	description			= models.TextField()
	allowed_plans		= models.ManyToManyField(Plans)


	def __str__(self):
		return self.title


	def get_absolute_url(self):
		return reverse('Series:series_detail', kwargs={'slug': self.slug})


	@property
	def episodes(self):
		return self.episodes_set.all().order_by("position")
	


	class Meta:
		verbose_name			= 'Series'
		verbose_name_plural		= 'Series'



class Episodes(models.Model):
	slug 				= models.SlugField()
	episode_title		= models.CharField(max_length=100)
	series 				= models.ForeignKey(Series, on_delete=models.SET_NULL, null=True)
	position			= models.IntegerField()
	video_url			= models.CharField(max_length=200)
	thumbnail			= models.ImageField()


	def __str__(self):
		return self.episode_title



	def get_absolute_url(self):
		return reverse('Series:episode_detail',
						kwargs={
								'series_slug': self.series.slug,
								'episode_slug': self.slug
						})		



	class Meta:
		verbose_name 			= 'Episode'
		verbose_name_plural		= 'Episodes'	