from django.contrib import admin

from .models import Series, Episodes
# Register your models here.
admin.site.register(Series)
admin.site.register(Episodes)