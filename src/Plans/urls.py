from django.urls import path

from .views import PlanSelectView, paymentView, updateTransaction, profileView, cancleSub

app_name = 'Plans'

urlpatterns = [
    path('', PlanSelectView.as_view(), name='plan_list'),
    path('payment/', paymentView, name='payment'),
    path('update-transactions/<subscription_id>/', updateTransaction, name='update-transactions' ), 
    path('profile/', profileView, name='profile'),
    path('cancel/', cancleSub, name="cancel")
]
