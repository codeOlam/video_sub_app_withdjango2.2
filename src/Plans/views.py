from django.conf import settings
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.views.generic import ListView
from django.contrib import messages
from django.urls import reverse

import stripe

from .models import Plans, UserPlan, Subscription


def profileView(request):
	user_plan 		= get_user_plan(request)
	user_sub 		= get_user_sub(request)

	context 		= {
						'user_plan':user_plan,
						'user_sub':user_sub
					}

	templates 		= 'Plans/profile.html'


	return render(request, templates, context)


def get_user_plan(request):
	user_plan_qs 	= UserPlan.objects.filter(user=request.user)

	if user_plan_qs.exists():
		return user_plan_qs.first()
	return None


def get_user_sub(request):
	user_sub_qs 		= Subscription.objects.filter(
					user_plan=get_user_plan(request)
		)

	if user_sub_qs.exists():
		user_sub = user_plan_qs.first()
		return user_sub
	return None


def get_selected_plan(request):
	"""
		Method will only work if selected plan is in
		Session.
	"""
	plan_type 		= request.session['selected_plan_type']

	#Convert plan_type from a string to an object
	selected_plan_qs 	= Plans.objects.filter(
				plan_type=plan_type
			)

	if selected_plan_qs.exists():
		return selected_plan_qs.first()
	return None 


class PlanSelectView(ListView):
	"""
		This class will hold the select new plan form
		And takes you to the payment gate way
	"""
	model 			= Plans


	def get_context_data(self, *args, **kwargs):
		context 					= super().get_context_data(**kwargs)
		current_plan 				= get_user_plan(self.request)
		context['current_plan'] 	= str(current_plan.plan)

		return context



	def post(self, request, **kwargs):
		selected_plan_type 		= request.POST.get('plan_type')

		user_plan 			= get_user_plan(request)
		user_sub 			= get_user_sub(request)

		# To check if user plan == selected plan
		selected_plan_qs 	= Plans.objects.filter(
				plan_type=selected_plan_type
			)

		if selected_plan_qs.exists():
			selected_plan 	= selected_plan_qs.first()


		#=======Validation handling =============

		if user_plan.plan == selected_plan:
			if user_sub != None:
				messages.info(request, "You already have this membership. Your \
					next payment is due {}".format('get value from strip'))

				return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

		# Pass selected plan into session

		request.session['selected_plan_type'] 	= selected_plan.plan_type

		return HttpResponseRedirect(reverse('Plans:payment'))


def paymentView(request):

	templates 		= "Plans/plan_payment.html"

	user_plan 		= get_user_plan(request)
	selected_plan 	= get_selected_plan(request)


	#Stripe subscription object and api
	if request.method == "POST":
		try:
			token = request.POST['stripeToken']
			subscription 	= stripe.Subscription.create(
				customer	= user_plan.stripe_user_id,
				items		= [{"plan": selected_plan.strip_plan_id}],
				source		= token  #4242424242424242, 12/19, 121, 21321
			)

			return redirect(reverse("Plans:update-transactions", 
									kwargs={
										'subscription_id':subscription.id
									}))

		except:
			messages.info(request, "Error getting information from your card!")


	# Find publish key
	# Pass to context for form usage
	publishKey 		= settings.STRIPE_PUBLISHABLE_KEY

	context 		= {
						'publishKey': publishKey,
						'selected_plan': selected_plan
					}

	return render(request, templates, context)


def updateTransaction(request, subscription_id):
	"""
		Update payment on our own backend
		Even though is is updated in strip back end
	"""
	user_plan 		= get_user_plan(request)
	selected_plan 	= get_selected_plan(request)

	user_plan.plan 	= selected_plan
	selected_plan.save()

	sub, created 		= Subscription.objects.get_or_create(user_plan=user_plan)
	sub.stripe_sub_id 	= subscription_id
	sub.active 			= True
	sub.save()

	try:
		del request.session['selected_plan_type']
	except:
		pass


	messages.info(request, "Successfully selected {} plan".format(selected_plan))

	return redirect('/')


def cancleSub(request):
	user_sub 		= get_user_sub(request)

	if user_sub.active == False:
		messages.info(request, "No active Plan")

		return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

	sub  	= stripe.Subscription.retrieve(user_sub.stripe_sub_id)
	sub.delete()

	user_sub.active == False
	user_sub.save()


	free_plan 		= Plans.objects.filter(plan_type='Hacker plan').first()
	user_plan 		= get_user_plan(request)
	user_plan.plan 	= free_plan
	user_plan.save()


	messages.info(request, "Successfully Cancelled Plan")

	return redirect("/plans/")