from django.contrib import admin

# Register your models here.
from Plans.models import Plans, UserPlan, Subscription


admin.site.register(Plans)
admin.site.register(UserPlan)
admin.site.register(Subscription)