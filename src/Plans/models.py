from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from datetime import datetime
import stripe

stripe.api_key		= settings.STRIPE_SECRET_KEY


PLAN_CHOICES = (
		('Select_plan', 'Select_plan'),
		('Hacker plan', 'HACKER PLAN'),
		('Weekly', 'WEEKLY'),
		('Monthly', 'MONTHLY'),
		('Yearly', 'YEARLY')
	)
# Create your models here.
class Plans(models.Model):
	slug			= models.SlugField()
	plan_type		= models.CharField(max_length=50, choices=PLAN_CHOICES, default="Hacker plan")
	price			= models.IntegerField(default=10)
	strip_plan_id	= models.CharField(max_length=50)


	def __str__(self):
		return 	self.plan_type


	class Meta:
		verbose_name = "Active Plan"
		verbose_name_plural = "Active Plans"



class UserPlan(models.Model):
	user 			= models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	stripe_user_id 	= models.CharField(max_length=50)
	plan 			= models.ForeignKey(Plans, on_delete=models.SET_NULL, null=True)


	def __str__(self):
		return self.user.username



class Subscription (models.Model):
	user_plan 		= models.ForeignKey(UserPlan, on_delete=models.CASCADE)
	stripe_sub_id 	= models.CharField(max_length=50)
	active			= models.BooleanField(default=True)


	def __str__(Self):
		return self.user_plan.user.username


	@property
	def get_created_date(self):
		sub 		= stripe.Subscription.retrieve(self.stripe_sub_id)
		sub_created = datetime.fromtimestamp(sub.created) 	

		return sub_created 


	@property 
	def get_next_billing_date(self):
		sub 			= stripe.Subscription.retrieve(self.stripe_user_id)
		sub_bill_due 	= datetime.fromtimestamp(sub.current_period_end)

		return sub_bill_due


def userplan_post_save_create(sender, instance, created, *args, **kwargs):
	if created:
		UserPlan.objects.get_or_create(user=instance)

	user_plan, created = UserPlan.objects.get_or_create(user=instance)

	if user_plan.stripe_user_id is None or user_plan.stripe_user_id == "":
		new_user_id = stripe.Customer.create(email=instance.email)
		user_plan.stripe_user_id = new_user_id['id']
		user_plan.save()

post_save.connect(userplan_post_save_create, sender=settings.AUTH_USER_MODEL)