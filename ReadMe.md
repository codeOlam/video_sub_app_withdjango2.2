![Django Logo](https://cdn0.froala.com/assets/editor/pages/B/frameworks/large/django-54a8740593da0031831666b94c418bc9.jpg)


## About
Django is a web-framework written in Python and runs the backend for many of the internet's most popular websites. This app is built ontop of Django 2.2.3. This app is made to look a little bit like Netflix. 


This app features the following:
-- You manage series based on subscription plans 

-- You can delete series or just an episode from a series

-- Uses stripe for payment system

-- Django custom authentication

I made use of Django CustomUserModel; this is advised when starting a project incase you need to extend the apllications authentication during development or in production. so the basic features like, LOGIN, LOGOUT, SIGNUP, VIEW USER PROFILE and UPDATE profile was not implemented in this project. this will be done in other future projects


## Technology and Requirements
1. Django 2.2.3
2. Python3
3. postgresql(using pyscopg2 as the adapter for python3)
4. Stripe for payment handling
5. Boostrap 3 (for front end)

## Installations
1. [installing Python3](https://www.python.org/downloads/)
2. [installing Django 2.2](https://docs.djangoproject.com/en/2.2/topics/install/)
3. [installing Virtualenv](https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/)
4. [installing and setting up Postgres](http://www.techken.in/how-to/install-postgresql-10-windows-10-linux/)
5. installing requirements from requirements.txt. After activating vitualenv run:

`(venv)path/to/video_sub_app/src$ pip install -r requirements.txt
`

6. [psycopg2](http://initd.org/psycopg/docs/install.html)
7. [Setting up Stripe account](https://stripe.com/)


## Run App
1. make sure your virtualenv is ativated.

`
(venv)path/to/video_sub_app/src$
`

2. make sure you are in the same directory where manage.py is then run

`(venv)path/to/video_sub_app/src$ python manage.py runserver
`

3. go to your web browser and enter 127.0.0.1:8000 

## Recommendations

Few recommendations are to experiment and build on top of this app, you can fork it. or use the knowledge to solve your own challenges. you can add more beauty to the front-end, and other cool features like; 

1. Upgrade the UI 
2. Could use paypal in place of stripe
3. create permission for only registered users to able to CREATE and UPDATE 

## Resources
1. [Django 2.2.3 Doc](https://docs.djangoproject.com/en/2.2/)
2. [JustDjango Youtube](https://www.youtube.com/watch?v=zu2PBUHMEew).

### Other Resources
1. [Custom User Model](https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project) 
2. [Custom Authentication](https://www.pythoncircle.com/post/28/creating-custom-user-model-and-custom-authentication-in-django/)
3. [Password and authentication in django](https://hackernoon.com/django-authentication-login-logout-and-password-change-reset-a5f3d9313cd5)
4. [boostrap doc](https://getbootstrap.com/docs/4.3/layout/grid/)
5. [bootsrap cdn](https://www.bootstrapcdn.com/)
